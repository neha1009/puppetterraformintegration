# Define the provider (AWS)
provider "aws" {
  access_key = "<accessskeyremoved>"
  secret_key = "<secretkeyremoved>"
  region     = "us-east-1"
  
}

# Define the EC2 instance resource
resource "aws_instance" "example_instance" {
  ami           = "ami-0cd59ecaf368e5ccf"  # Example AMI ID
  instance_type = "t2.micro"               # Example instance type
  key_name      = "puppet"                 # Name of your SSH key pair in AWS

  # Define the connection settings for SSH
  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("C:/Users/farur/Downloads/puppet.pem")  # Path to your private key file
    host        = "ec2-34-224-57-72.compute-1.amazonaws.com"   # Public DNS of the instance
  }

  # Add a remote-exec provisioner to install and configure Puppet
  provisioner "remote-exec" {
    inline = [
      "sudo apt update",
      "sudo wget https://apt.puppetlabs.com/puppet-release-bionic.deb",
      "sudo dpkg -i puppet-release-bionic.deb",
      "sudo apt update",
      "sudo apt install puppet-agent -y",
      "sudo sh -c 'echo \"34.224.57.72 puppet\" >> /etc/hosts'",
      "sudo systemctl start puppet",
      "sudo systemctl enable puppet",
      "sudo puppet agent --test"
    ]
  }
}

output "public_ip" {
  description = "The public IP address of the newly created pupppet instance"
  value       = aws_instance.example_instance.public_ip
}



